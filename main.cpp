﻿#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cwctype>

#include "Bayes.h"

int main() {
	std::wfstream fs;
	fs.open("SMSSpamCollection.txt");
	if (!fs.is_open())
		return 1;
	std::wstring message;
	Bayes bayes;
	while (std::getline(fs, message)) {
		std::transform(message.begin(), message.end(), message.begin(), ::towlower);
		message.erase(std::remove_if(message.begin(), message.end(),::iswpunct), message.end());
		std::wistringstream ss(message);
		Bayes::Mail res(std::istream_iterator<std::wstring, wchar_t>(ss), {});
		if (res[0] == L"ham")
			bayes.learnHam(res);
		else
			bayes.learnSpam(res);
	}
	
	size_t totalSpam = 0;
	size_t totalHam = 0;
	size_t hamasham = 0;
	size_t spamasspam = 0;

	std::wfstream fs1;
	fs1.open("tst.txt");
	if (!fs1.is_open())
		return 1;
	std::wstring message1;
	while (std::getline(fs1, message1)) {
		std::transform(message1.begin(), message1.end(), message1.begin(), ::towlower);
		message1.erase(std::remove_if(message1.begin(), message1.end(), ::iswpunct), message1.end());
		std::wistringstream ss(message1);
		Bayes::Mail res(std::istream_iterator<std::wstring, wchar_t>(ss), {});
		bool isSpam = false;
		if (res[0] == L"ham") {
			totalHam++;
			std::cout << "ham\n";
		}
			
		else {
			std::cout << "spam\n";
			isSpam = true;
			totalSpam++;
		}
		//std::wcout << res[0] << '\n';
		res.erase(res.begin());
		bool result = bayes.isSpam(res);
		std::cout << (result == 1 ? ("predicted as spam\n\n") : ("predicted as ham\n\n"));
		if (result && isSpam) {
			spamasspam++;
		}
		else if (!result && !isSpam) {
			hamasham++;
		}
	}
	std::cout << "accuracy: " << ((spamasspam + hamasham) * 1.0 / (totalHam + totalSpam)) * 100;

}