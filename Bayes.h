#ifndef BAYES_H
#define BAYES_H

#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>

class Bayes final {
public:
	using Mail = std::vector<std::wstring>;

	Bayes() = default;
	void learnHam(Mail& msg);
	void learnSpam(Mail& msg);

	bool isSpam(Mail& msg);
	
private:

	//� �������� ���� ���������� ����������� �����
	std::unordered_map<std::wstring, size_t> spamWordsOccurs_;
	//� �������� "����������" ���������� ����������� �����
	std::unordered_map<std::wstring, size_t> hamWordsOccurs_;

	size_t totalSpamMessages_ = 0;
	size_t totalHamMessages_ = 0;


	void remove_duplicates(Mail& msg)
	{
		std::sort(msg.begin(), msg.end());
		msg.erase(std::unique(msg.begin(), msg.end()), msg.end());
	}
	
};

#endif
