#include <string>
#include <vector>
#include <numeric>
#include <iostream>
#include <list>
#include <cmath>

#include "Bayes.h"

void Bayes::learnHam(Mail& msg) {
	remove_duplicates(msg);
	totalHamMessages_++;

	for (const auto& word : msg)
		hamWordsOccurs_[word]++;

}

void Bayes::learnSpam(Mail& msg) {
	remove_duplicates(msg);
	totalSpamMessages_++;

	for (const auto& word : msg)
		spamWordsOccurs_[word]++;
}

bool Bayes::isSpam(Mail& msg) {
	static constexpr double minProb = 0.7;
	std::vector<double> spamness;
	for (const auto& word : msg) {
		double currWordFreqInSpamMsg = (static_cast<double>(spamWordsOccurs_[word]) + 1.) / (static_cast<double>(totalSpamMessages_) + 1.);
		double currWordFreqInHamMsg = (static_cast<double>(hamWordsOccurs_[word]) + 1.) / (static_cast<double>(totalHamMessages_) + 1.);
		double currWordSpamProb = (currWordFreqInSpamMsg) / (currWordFreqInSpamMsg + currWordFreqInHamMsg);

		spamness.push_back(currWordSpamProb);
	}

	//������� �����������
	//��������������� �����, ����� �� ���� float-point underflow
	double tetta = std::accumulate(spamness.begin(), spamness.end(), 0., [](double prevItersResult, double currSpammnes) {
		return prevItersResult + (std::log(1 - currSpammnes) - std::log(currSpammnes));
		});

	double result = 1. / (1. + std::exp(tetta));
	std::cout << result << '\n';

	if (result > minProb) {
		return true;
	}

	return false;
}
